from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('',
    (r'^register/', 'views.register'),
    (r'^confirm/(?P<activation_key>.)', 'views.confirm'),
)

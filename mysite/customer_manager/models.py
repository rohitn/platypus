from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Customer(models.Model):
  CUSTOMER_TYPES = (
        ('User', 'User'),
        ('Advertiser', 'Advertiser'),
        ('Admin', 'Admin')
    )
  first_name = models.CharField(max_length=30)
  last_name = models.CharField(max_length=30)
  email = models.EmailField(max_length=75)
  username = models.CharField(max_length=30)
  activation_key = models.CharField(max_length=40)
  key_expires = models.DateTimeField()
  customer_type = models.CharField(max_length=10, choices=CUSTOMER_TYPES)
  creation_time = models.DateTimeField()
  modification_time = models.DateTimeField(auto_now=True)

class UserProfile(models.Model):
    user = models.OneToOneField(User)
    activation_key = models.CharField(max_length=40)
    key_expires = models.DateTimeField()